Petfee is an automatic pet feeder using Raspberry Pi.
- Features : 
    - Integrated to Web Application System
    - Auto feed mode using `Cron Job Scheduler`
    - Manual feed mode using press button