import RPi.GPIO as GPIO
import time

def blink(pin):
	GPIO.output(pin,GPIO.HIGH)
def blinkoff(pin):
	GPIO.output(pin,GPIO.LOW)

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)

p = GPIO.PWM(12, 50)

p.start(7.5)

try:
#        while True:
		blink(11)
                p.ChangeDutyCycle(2.5)  # turn towards 90 degree
                time.sleep(1.0) # sleep 1 second
                #p.ChangeDutyCycle(2.5)  # turn towards 0 degree
                #time.sleep(1) # sleep 1 second
                p.ChangeDutyCycle(12.5) # turn towards 180 degree
                time.sleep(1) # sleep 1 second 
 		p.stop()
		blinkoff(11)
        	GPIO.cleanup()
except KeyboardInterrupt:
        p.stop()
        GPIO.cleanup()
